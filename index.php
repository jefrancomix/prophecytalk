<?php

require_once 'vendor/autoload.php';

use Aura\Router\RouterContainer;
use Pimple\Container;
use Zend\Diactoros\ServerRequestFactory;

$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();
$map->get('blog.read', '/blog/{id}');
$map->get('main.index', '/');
$map->get('main.getPokemon', '/pokemon/{id}');
$map->post('main.searchPokemon', '/search-pokemon');

$request = ServerRequestFactory::fromGlobals(
  $_SERVER,
  $_GET,
  $_POST,
  $_COOKIE,
  $_FILES
);

$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);
$no_exists_message = "No encontramos ese pokemon.";
if (!$route) {
  print $no_exists_message;
  exit;
}

list($controller, $action) = explode('.', $route->handler);
if (isset($route->attributes['action'])) {
  $action = $route->attributes['action'];
  unset($route->attributes['action']);
}
$controller = ucfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $controller))));
$controller = "\\PHPmx\\Controllers\\{$controller}";
$action = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $action))));
if (!class_exists($controller)) {
  print $no_exists_message;
}

$container = new Container();
$container['twig'] = function($c) {
  return new Twig_Environment((new Twig_Loader_Filesystem(dirname(__FILE__) . '/src/Views')), [
    'cache' => dirname(__FILE__) . '/cache',
    'debug' => true
  ]);
};

$attributes = array_values(array_merge($route->attributes, [$request]));
print call_user_func_array([new $controller($container), $action], $attributes);


