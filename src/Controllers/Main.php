<?php

namespace PHPmx\Controllers;

use PokemonAPI\Pokemon;
use PokemonAPI\Sprite;

class Main {

  /**
   * @var \Pimple\Container
   */
  var $container;

  public function __construct(\ArrayAccess $container) {
    $this->container = $container;
  }

  public function index(): string {
    return $this->container['twig']->render('index.twig');
  }

  /**
   * @param \Psr\Http\Message\ServerRequestInterface $request
   * @return bool
   */
  public function searchPokemon(\Psr\Http\Message\ServerRequestInterface $request) {
    $search = $request->getParsedBody();
    $pokemon = new Pokemon($search['search']);

    if (get_class($pokemon) == 'PokemonAPI\Pokemon') {
      header('Location: /pokemon/' . $pokemon->getNationalId());
      return true;
    }
    header('Location: /');
    return false;
  }

  /**
   * Search the pokemon
   * @param int $national_id Pokemon ID.
   * @return mixed
   */
  public function getPokemon(int $national_id) {
    $pokemon = new Pokemon($national_id);
    $avatar = 'https://pokeapi.co' . (new Sprite($pokemon->sprites[0]->resource_id))->image;
    return $this->container['twig']->render('pokemon.twig', ['pokemon' => $pokemon, 'avatar' => $avatar]);
  }
}