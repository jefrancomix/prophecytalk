<?php

use PHPmx\Controllers\Main;
use Prophecy\Prophet;

class MainTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $prophet;

    protected function _before()
    {
      $this->prophet = new Prophecy\Prophet;
    }

    protected function _after()
    {
      $this->prophet->checkPredictions();
    }

    public function testSearchPokemon()
    {
      // Dummy.
      $request = $this->prophet->prophesize('\Psr\Http\Message\ServerRequestInterface');
      // Stub.
      $request->getParsedBody()->willReturn(['search' => 25]);
      // Mock.
      $request->getParsedBody()->shouldBeCalled();

      $arrayAccess = $this->prophet->prophesize('\ArrayAccess');

      $controller = new Main($arrayAccess->reveal());
      $this->assertTrue($controller->searchPokemon($request->reveal()));
    }


  public function testGetPokemon() {
    $this->prophet = new Prophecy\Prophet;
    $twig = new class {
      public function render($template, $params) {
        return ['template' => $template, 'params' => $params];
      }
    };

    $arrayAccess = $this->prophet->prophesize('\ArrayAccess') ;
    $arrayAccess->offsetExists('twig')->willReturn(true);
    $arrayAccess->offsetGet('twig')->willReturn($twig);
    $controller = new Main($arrayAccess->reveal());
    $output = $controller->getPokemon(25);

    $this->assertTrue($output['template'] == 'pokemon.twig');
    $this->assertTrue($output['params']['pokemon']->name == 'Pikachu');
    $this->assertTrue($output['params']['pokemon']->national_id == 25);

  }
}